# 2024-01-31 Notes taking template/ Develop and enrich Software Heritage’s Wikipedia pages

- Leads: Cécile Arènes and Océane Valencia

https://hedgedoc.softwareheritage.org/OYyNMvKPRQKfdRECFXkvcA?both#

## List of attendees
- 

## 3 key messages for the report back

### Idea #1

### Idea #2

### Idea #3

## Pictures (optional)

No picture of people but picture of posters, any printed material, etc. that can help to understand the work done.
