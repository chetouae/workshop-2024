---
tags: ambassadors, community
---
# 🤝 Delivering a workshop for the 2024 community event

We offer to the ambassadors up to 5 slots to deliver a session for a small group during the next Software Heritage community workshop. 

 ↪️ An option could be:
- you have an idea of activity but you wouldn't lead it (for any reason) : then, you can add your proposal in the pad too.
Thus, we'll check if someone from the community could turn your idea into reality.

Looking forward to discover your proposals! 

[TOC]

----

## Practical info about the community event
- Jan. 31st, 2024
- Inria Centre, 2 rue Simone Iff, Paris
- in person event, no hybrid mode
- Registration link: https://framaforms.org/2024-software-heritage-community-workshop-1701894412

## What? 
- 45' activity
- target audience: the whole Software Heritage community = devs, project-managers, researchers from all academic fields, advocates, archivists and librarians interested in Software Heritage.
- Max. number of participants: 10
- You are encouraged to put the accent on active work sessions rather than on presentations without interactions ; your audience should be in an active position
- possible topics: use cases, teaching practices, outreaching and more related to Software Heritage.
- possible to offer an online activity: there will be no hybrid mode, but it will be possible to have a 100% online workshop. Please, add it in the description. 

Option: deliver an online workshop later on 

Possible to deliver an activity in another language than EN (FR for instance)

Proposals for the types of outputs we could produce:
- (Agustin) blogposts
- (Agustin) the most effective way to produce outputs: in KDE works very well to end each one of the community days with a summary of each activity by the coordinator (2-3' summary and Q&A). This activity is recorded and the video published within the community, not publicly. 
- SO it is the closure of each day, a live summary

## How to make a proposal of activity?
- **deadline to make a proposal**: **Dec. 15th**
- title of your workshop
- short description of the workshop
- objectives related to Software Heritage: how could we go further for Software Heritage? 
- would you need to be in touch with the technical team, to do some checking before the workshop? Y/N
- joint ventures between ambassadors are welcome
- which type of output will be produced during the workshop? e.g.: canvas for a cheat sheet, standardized answers, check list to improve documentation? Many types of outputs may be created. 💡 Tip: when you design your activity, anticipate the way information will be gathered after the workshop, in order to make this output alive in the easiest way for you. Thus, creating pads in advance for instance may be a good thing. 

## Proposals
### Proposal 1: Develop and enrich Software Heritage's Wikipedia pages
- Who made the proposal? Add your name: Océane and Cécile
- title of your workshop: **Develop and enrich Software Heritage's Wikipedia pages**
- short description of the workshop (1 paragraph is fine) : The aim of this workshop is to contribute to the completion and updating of the Software Heritage Wikipedia pages. The French and English Wikipedia pages on Software Heritage lack information and need to be updated. Many references could be added to improve the quality of the page. Concepts could be developed to present the different aspects of Software Heritage. 
- objectives related to Software Heritage: how could we go further for Software Heritage? The objective of this work is to contribute to a better understanding of Software Heritage. Many people are still unaware that code and software is a natively digital heritage that needs to be preserved and enhanced. Raising awareness of Software Heritage is fundamental to the development of code and software preservation.
- would you need to be in touch with the technical team, to do some checking? N
- any prerequisites for the contributors? may be technical or not (e.g.: having an account on the platform A, bringing a laptop, etc.): create a Wikipedia account https://en.wikipedia.org/w/index.php?title=Special:CreateAccount&returnto=Wikipedia%3AWhy+create+an+account%3F 
- in-person

### Proposal 2: Define use cases for industry to create collaterals
- Who made the proposal? Add your name: Agustín Benito Bethencourt
- title of your workshop: **Use cases for industry to create collaterals**
- short description of the workshop (1 paragraph is fine) : SwH has an outstanding potential for industry use cases. In order to attract the attention of leaders in different verticals, we need content based on use cases they understand, including a problem statement and which benefit SwH brings to mitigate or even solve those challenges. The initial steps to build these use cases can be found here: https://gitlab.softwareheritage.org/swh/meta/-/issues/5049
- objectives related to Software Heritage: how could we go further for Software Heritage? The goal should be to push the current status further, ideally creating the content neccessary to build collaterals from.
- would you need to be in touch with the technical team, to do some checking? Y
    - I am not familiar with some of the SwH services for companies. The discussion will benefit from somebody with technical product (marketing) knowledge to define the offering that we might provide to mitigate the problem statement points for each of the use cases.
- any prerequisites for the contributors? read and follow https://gitlab.softwareheritage.org/swh/meta/-/issues/5049 Comment on the existing proposal is welcome
- in-person

### Proposal 3: Using the Software Heritage to harvest institution's entries
- Who made the proposal? Add your name: Violaine Louvet
- title of your workshop: **Harvesting Software Heritage**
- short description of the workshop (1 paragraph is fine) : The idea is to understand how we can harvest SWH Database in order to identify entries related to a specific institution
- objectives related to Software Heritage: how could we go further for Software Heritage? Identify what is possible and what is not for the moment.
- would you need to be in touch with the technical team, to do some checking? Y
- any prerequisites for the contributors? Be familiar with Python and notebooks (l'idée serait de faire les scripts en python sur une plateforme de notebooks) to be confirmed by the technical team
- in-person


### Proposal 4: WIP

- Who made the proposal? Add your name: Harish
- title of your workshop: 
- short description of the workshop (1 paragraph is fine) : 
- objectives related to Software Heritage: how could we go further for Software Heritage? 
- would you need to be in touch with the technical team, to do some checking? Y/N
- any prerequisites for the contributors? 
- **online**

### Proposal 5: Software Heritage for editorial offices
- Who made the proposal? Pierre
- title of your workshop: **Software Heritage for editorial offices**
- short description of the workshop (1 paragraph is fine) : Editorial offices of academic journals are in charge of processing accepted papers to a publication-ready form. They usualy check for typography, figures format, references style... Since recently, they also verify authors comply with the journal policy regarding open science, notably for the sharing of data and code. People from these editorial offices may not be aware of Software Heritage and its missions. They could for instance ask the archiving of source code in Zenodo rathe than in SWH.
- objectives related to Software Heritage:
    - Write a small text for editorial offices to explain what is SWH and what its missions are.
    - Write a larger text (a web page ?) gathering ressources and tips for editorial offices to check source code is appropriatly archived in SWH and cited with SWHID.
- would you need to be in touch with the technical team, to do some checking? N
- any prerequisites for the contributors? Ideally, having an experience in the publication process of a research article and some interactions with the editorial office of an academic journal.
- in-person/online
- for the record, here is the small text I wrote and we could build upon:
    > Software Heritage (https://www.softwareheritage.org/) is the universal archive for source code. It's a UNESCO sponsored organization which goals are to collect and preserve software in source code. Software Heritage stores more than 265 million projects. It's is a reliable and long-lasting solution better suited for archiving source code than data respositories like Zenodo or Figshare. Software Heritage provides a unique cryptographic persistent intrinsic identifier called SWHID (from “SoftWare Hash IDentifiers”, https://www.swhid.org/) that is related to a specific version of the software source code.

... to be continued!