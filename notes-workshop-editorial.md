# 2024-01-31 Notes taking template/ Software Heritage for editorial offices

Lead: Pierre Poulain


## List of attendees
- Andreas Enge
- Raphaël Tournoy
- Hélène Bégnis
- Morane Gruenpeter
- Pierre Poulain

## Workshop description
Editorial offices of academic journals are in charge of processing accepted papers to a publication-ready form. They usualy check for typography, figures format, references style… Since recently, they also verify authors comply with the journal policy regarding open science, notably for the sharing of data and code. People from these editorial offices may not be aware of Software Heritage and its missions. They could for instance ask the archiving of source code in Zenodo rathe than in SWH.

objectives related to Software Heritage:
- Write a small text for editorial offices to explain what is SWH and what its missions are.
- Write a larger text (a web page ?) gathering ressources and tips for editorial offices to check source code is appropriatly archived in SWH and cited with SWHID.


would you need to be in touch with the technical team, to do some checking? N
any prerequisites for the contributors? Ideally, having an experience in the publication process of a research article and some interactions with the editorial office of an academic journal.
in-person/online

## Output

Software Heritage (https://www.softwareheritage.org/) is the universal archive specialized in the preservation of source code. It's a UNESCO supported organization whose goals are to collect and share software in source code.

Software Heritage stores more than 265 million projects. It is a reliable and long-lasting solution better suited for archiving source code than data respositories like Zenodo or Figshare. Software Heritage provides a unique persistent identifier called SWHID (from “SoftWare Hash IDentifiers”, https://www.swhid.org/) that is related to a specific version of the software source code and cannot be altered.

## Next step
- [ ] In March/April, Pierre and Morane edit the text

## Pictures (optional)
No picture of people but picture of posters, any printed material, etc. that can help to understand the work done.
