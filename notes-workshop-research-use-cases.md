https://hedgedoc.softwareheritage.org/s/2024-01-31-research#

# Using SWH for research purpose

Lead: Romain Lefeuvre romain.lefeuvre(at)inria.fr

## For what purpose a researchers might be interested in using Software Heritage for their research ?

- Generic BigData analysis/usage : SWH might be considered as any huge database for performing big data analysis (that are not necessary focused on source code).
    - Train or test machine learning algorithms on a real world database

- Software Engineering analysis/usage : querying the universal software archive to support software engineering studies.

- Archiving scientific artefacts
    - Citing : Intrinsic Immutable identifier - SWHID
    - Saving : Using the archive itself as a way to provide access to your code.

## Kinds of query discussed

- Querying the graph metadata

- Querying the content of the file at large scale
    - Pattern extraction of code etc…
    - Get all the readme of all the repositories

- Querying both metadata and file content
    - Identify repositories that shared history, source code pattern

- Querying a priori defined subset of the graph
    - Specific origins
    - Specific time interval
    - Granularity of the history more recent commit…

## Research use cases discussed

- Research on the evolution of software development practices
    - For instance : Studying the evolution of programmation style
    - Studying software providing the same features …

- Empirical Research
    - Mining Software Repository

- Classify open source software based on their functionality
    - Compute and add metadata to the graph in order to classify the different software of the archive

- Identify usage of vulnerable code
    - The global graph nature of SWH enable analysis that are not possible with classical (local) approach

- Create specialized graph/datasets for different domains of research
    - Maths Software, Scientific Software, physics Software, 3D Software.

## Issue that prevent researcher to use SWH for their research

- The code itself cannot be queried at scale
- Querying the graph might be time-consuming for someone unfamiliar with swh-graph APIs.
- At a certain point the complexity of your analysis requires you to deploy an instance of swh-graph on your infrastructure.
    - There is a gap between small analysis that can be performed via web/graphql api and resource intensive traversal that worth the investment to deploy locally the graph.
- Analysis on identified subset of the graph need to be performed on the entire graph

## Discussed solution

- Facilitating resource intensive analysis by providing scripts to deploy automatically swh-graph on external resources (Grid5k, cloud provider)

- Facilitating the creation of sub-graph of swh-graph to execute it on low profile systems
    - Providing the resources (or the scripts to be used on GRID 5k / cloud provider) to compute sub graph

- Provide a more abstract way to query the graph
    - Query language
